/*
 * gstreamer_video.h
 *
 *  Created on: Apr 7, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_GSTREAMER_GSTREAMER_VIDEO_H_
#define MODULES_GSTREAMER_GSTREAMER_VIDEO_H_

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/app/gstappsink.h>

#include <gst/app/gstappsink.h>
#include <gst/video/video.h>
#include <glib-object.h>
#include <glib.h>
#include <algorithm>

#include "scene/2d/node_2d.h"

class GDGstAppSink {
public:
	virtual GstFlowReturn	on_preroll(GstBuffer * buffer){
		return GST_FLOW_OK;
	}
	virtual GstFlowReturn	on_buffer(GstBuffer * buffer){
		return GST_FLOW_OK;
	}
	virtual void			on_eos(){}

	// return true to set the message as attended so upstream doesn't try to process it
	virtual bool on_message(GstMessage* msg){return false;};

	// pings when enough data has arrived to be able to get sink properties
	virtual void on_stream_prepared(){};
};

class GstreamerVideo : public Node2D {
private:
	GDCLASS( GstreamerVideo, Node2D ); // @suppress("Symbol is not resolved")

	void 				gstHandleMessage();
//	void				update(ofEventArgs & args);
	bool				startPipeline();

	bool 				bPlaying;
	bool 				bPaused;
	bool				bIsMovieDone;
	bool 				bLoaded;
	bool 				bFrameByFrame;
	int					loopMode;

	GstElement  *		gstSink;
	GstElement 	*		gstPipeline;
	GDGstAppSink * 		appsink;

	bool				posChangingPaused;
	int					pipelineState;
	float				speed;
	gint64				durationNanos;
	bool				isAppSink;
	bool				isStream;

protected:
	static void _bind_methods();

public:

//	bool 	setPipelineWithSink(std::string pipeline, std::string sinkname="sink", bool isStream=false);
//	bool 	setPipelineWithSink(GstElement * pipeline, GstElement * sink, bool isStream=false);
//	void 	play();
//	void 	stop();
//	void 	setPaused(bool bPause);
//	bool 	isPaused(){return bPaused;}
//	bool 	isLoaded(){return bLoaded;}
//	bool 	isPlaying(){return bPlaying;}
//	float	getPosition();
//	float 	getSpeed();
//	float 	getDuration();
//	guint64 getDurationNanos();
//	bool  	getIsMovieDone();
//	void 	setPosition(float pct);
//	void 	setVolume(int volume);
//	void 	setLoopState(ofLoopType state);
//	int		getLoopState(){return loopMode;}
//	void 	setSpeed(float speed);
//	void 	setFrameByFrame(bool bFrameByFrame);
//	bool	isFrameByFrame();
//	GstElement 	* getPipeline();
//	GstElement 	* getSink();
//	unsigned long getMinLatencyNanos();
//	unsigned long getMaxLatencyNanos();
//	virtual void close();
//	void setSinkListener(ofGstAppSink * appsink);
//	// callbacks to get called from gstreamer
//	virtual GstFlowReturn preroll_cb(GstBuffer * buffer);
//	virtual GstFlowReturn buffer_cb(GstBuffer * buffer);
//	virtual void 		  eos_cb();

	GstreamerVideo();
	virtual ~GstreamerVideo();

	// the gst callbacks need to be friended to be able to call us
	//friend GstFlowReturn on_new_buffer_from_source (GstAppSink * elt, void * data);
	//friend GstFlowReturn on_new_preroll_from_source (GstAppSink * elt, void * data);
	//friend void on_eos_from_source (GstAppSink * elt, void * data);

};

#endif /* MODULES_GSTREAMER_GSTREAMER_VIDEO_H_ */
