/*
 * gstreamer_video.cpp
 *
 *  Created on: Apr 7, 2021
 *      Author: frankiezafe
 */

#include "gstreamer_video.h"

static bool plugin_registered = false;
static bool gst_inited = false;

// called when the appsink notifies us that there is a new buffer ready for
// processing

//static GstFlowReturn on_new_buffer_from_source (GstAppSink * elt, void * data){
//	GstBuffer *buffer = gst_app_sink_pull_buffer (GST_APP_SINK (elt));
//	return ((GstreamerVideo*)data)->buffer_cb(buffer);
//}
//
//static GstFlowReturn on_new_preroll_from_source (GstAppSink * elt, void * data){
//	GstBuffer *buffer = gst_app_sink_pull_preroll(GST_APP_SINK (elt));
//	return ((GstreamerVideo*)data)->preroll_cb(buffer);
//}
//
//static void on_eos_from_source (GstAppSink * elt, void * data){
//	((GstreamerVideo*)data)->eos_cb();
//}
//
//static gboolean appsink_plugin_init (GstPlugin * plugin) {
//  gst_element_register (plugin, "appsink", GST_RANK_NONE, GST_TYPE_APP_SINK);
//  return TRUE;
//}


void GstreamerVideo::_bind_methods() {

}

GstreamerVideo::GstreamerVideo() {

	bLoaded 					= false;
	speed 						= 1;
	bPaused						= false;
	bIsMovieDone				= false;
	bPlaying					= false;
	loopMode					= 0;
	bFrameByFrame 				= false;
	gstPipeline					= NULL;
	gstSink						= NULL;
	posChangingPaused			= 0;
	durationNanos				= 0;
	isAppSink					= false;
	appsink						= NULL;

//	if(!g_thread_supported()){
//		g_thread_init(NULL);
//	}
//	if(!gst_inited){
//		gst_init (NULL, NULL);
//		gst_inited=true;
//		WARN_PRINT("GstreamerVideo: gstreamer inited");
//	}
//	if(!plugin_registered){
//		gst_plugin_register_static(GST_VERSION_MAJOR, GST_VERSION_MINOR,
//					"appsink", (char*)"Element application sink",
//					appsink_plugin_init, "0.1", "LGPL", "ofVideoPlayer", "openFrameworks",
//					"http://openframeworks.cc/");
//		plugin_registered=true;
//	}

}

GstreamerVideo::~GstreamerVideo() {
}

